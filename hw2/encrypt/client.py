# -*- coding: latin-1 -*-
#
# Copyright (C) AB Strakt
# Copyright (C) Jean-Paul Calderone
# See LICENSE for details.

"""
Simple SSL client, using blocking I/O
"""

from OpenSSL import SSL
import sys, os, select, socket, random, struct
from Crypto.Cipher import AES

def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print 'Got certificate: %s' % cert.get_subject()
    return ok

def encrypt_file_send(key,sock, in_filename, out_filename=None, chunksize=64*1024):
    """ Encrypts a file using AES (CBC mode) with the
        given key.

        key:
            The encryption key - a string that must be
            either 16, 24 or 32 bytes long. Longer keys
            are more secure.

        in_filename:
            Name of the input file

        out_filename:
            If None, '<in_filename>.enc' will be used.

        chunksize:
            Sets the size of the chunk which the function
            uses to read and encrypt the file. Larger chunk
            sizes can be faster for some files and machines.
            chunksize must be divisible by 16.
    """
    if not out_filename:
        out_filename = in_filename + '.enc'

    iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    filesize = os.path.getsize(in_filename)

    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
            #try:
            #	sock.send('RECEIVE'+' '+out_filename+' ')
	    #	#sys.stdout.write(sock.recv(20))
	    	#sys.stdout.flush()
	    #except SSL.Error:
           #	print 'Connection died unexpectedly'
            #outfile.write(struct.pack('<Q', filesize))
	    sock.send(struct.pack('<Q', filesize))
            #outfile.write(iv)
            sock.send(iv)

            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += ' ' * (16 - len(chunk) % 16)

                #outfile.write(encryptor.encrypt(chunk))	
                sock.send(encryptor.encrypt(chunk))


def decrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    """ Decrypts a file using AES (CBC mode) with the
        given key. Parameters are similar to encrypt_file,
        with one difference: out_filename, if not supplied
        will be in_filename without its last extension
        (i.e. if in_filename is 'aaa.zip.enc' then
        out_filename will be 'aaa.zip')
    """
    if not out_filename:
        out_filename = os.path.splitext(in_filename)[0]

    with open(in_filename, 'rb') as infile:
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        iv = infile.read(16)
        decryptor = AES.new(key, AES.MODE_CBC, iv)

        with open(out_filename, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(chunk))

            outfile.truncate(origsize)




if len(sys.argv) < 3:
    print 'Usage: python[2] client.py HOST PORT'
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
ctx = SSL.Context(SSL.TLSv1_METHOD)
ctx.set_verify(SSL.VERIFY_PEER, verify_cb) # Demand a certificate
ctx.use_privatekey_file (os.path.join(dir, 'client.pkey'))
ctx.use_certificate_file(os.path.join(dir, 'client.cert'))
ctx.load_verify_locations(os.path.join(dir, 'CA.cert'))

def chunks(lst, n):
    #Yield sucessive n-sized chuncks from string lst
    for i in xrange(0,len(lst),n):
        yield lst[i:i+n]    


# Set up client
sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
sock.connect((sys.argv[1], int(sys.argv[2])))


while True:
    	opt=raw_input("Please select send or receive\n 1 send, 2 receive\n")
	filename=raw_input("Enter your filename\n")
	
	if opt=='1':	## send file
	    fo=open(filename,"rb")
	    plaintextBuffer=''	
	    try:
            	sock.send('RECEIVE'+' '+filename+' ')
	    	sys.stdout.write(sock.recv(20))
	    	sys.stdout.flush()
	    except SSL.Error:
           	print 'Connection died unexpectedly'
                break
            
            while True:
                str = fo.read(1024)
                if str == '':
                    break
	        plaintextBuffer+=str
	    fo.close()
             
            enc_opt=raw_input('Please select whether you want to encrypt the file transfer or not\n Y+Enter for encrypted, N+Enter for non-encrypted\n')
		
	    if (enc_opt=='N' or enc_opt=='n'):
                for chunk in chunks(plaintextBuffer,1024):
		    try:
		        sock.send(chunk)
		    except SSL.Error:
		        print 'Connection died unexpectedly'
		        break
	        print 'File Transferred successfully\n'
	    elif (enc_opt=='Y' or enc_opt=='y'):
		#encryption part
		key="1234567890abcdef"
		
		encrypt_file_send(key,sock,filename,'ecopy_'+filename,1024)
		
			
		#decrypt_file(key,'copy_'+filename,'pcopy_'+filename,1024)













            #encryption part
            #key="1234567890abcdef"
		
	   # encrypt_file(key,filename,'ecopy_'+filename)

	    
            #decrypt_file(key,'ecopy_'+filename,'pcopy_'+filename)

	    #for chunk in chunks(plaintextBuffer,1024):
		#try:
		 #   sock.send(chunk)
		#except SSL.Error:
		 #   print 'Connection died unexpectedly'
		  #  break



	elif opt=='2':
	    buffer=''
	    try:
		sock.send('GET'+' '+filename+' ')
	    except SSL.Error:
 		print 'Connection died unexpetedly'
		break
            try:
                ret=sock.recv(20)
		sys.stdout.write(ret)
		sys.stdout.flush()
	    except SSL.Error:
		print 'SSL Connection Error'
	    while True:
		try:
		    ret=sock.recv(1024)
		except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                    pass
                except SSL.ZeroReturnError:		# reach EOF
		    print 'Fetched file successfully'
		    filename=raw_input('Please enter the file name you want to save:\n')
		    fo=open(filename,'wb')
		    fo.write(buffer)
		    fo.close()
		    dec_opt=raw_input('Do you want to decrypt this file?\n Press Y+Enter to enter password to decrypt, or enter other keys to quit\n')
                    if (dec_opt=='Y' or dec_opt=='y'):
                        key=raw_input('Please enter your key below:\n')
                        #key="1234567890abcdef"
                        decrypt_file(key,filename,'pcopy_'+filename,1024)
	  	    break
		else:
		    buffer=buffer+ret
	else:
	    print 'Wrong selection\nNo action will be taken\n'
	    pass

        
	# do the plaintext compare here

	cont=raw_input('Press 1 to continue otherwise quit:')
	if not cont=='1':
	    break;

sock.shutdown()
sock.close()