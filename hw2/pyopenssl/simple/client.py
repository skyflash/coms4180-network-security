# -*- coding: latin-1 -*-
# client.py
#
# Copyright (C) 2001 Martin Sj�gren and AB Strakt, All rights reserved
#
# $Id: client.py,v 1.7 2002/08/15 12:20:46 martin Exp $
#
"""
Simple SSL client, using blocking I/O
"""

from OpenSSL import SSL
import sys, os, select, socket

def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print 'Got certificate: %s' % cert.get_subject()
    return ok

if len(sys.argv) < 3:
    print 'Usage: python[2] client.py HOST PORT'
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
ctx = SSL.Context(SSL.TLSv1_METHOD)
ctx.set_verify(SSL.VERIFY_PEER, verify_cb) # Demand a certificate
ctx.use_privatekey_file (os.path.join(dir, 'client.pkey'))
ctx.use_certificate_file(os.path.join(dir, 'client.cert'))
ctx.load_verify_locations(os.path.join(dir, 'CA.cert'))

# Set up client
sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
sock.connect((sys.argv[1], int(sys.argv[2])))


while 1:
	opt=raw_input("Please select send or receive\n 1 send, 2 receive\n")
	filename=raw_input("Enter your filename\n")
	
	if opt=='1':	## send file
		fo=open(filename,"rb")
		
		# try:
			# sock.send('RECEIVE '+filename)
		# except SSL.Error:
			# print 'Connection died unexpectedly'
			# break

		while 1:
			# line = sys.stdin.readline()
			# if line == '':
				# break
			# try:
				# sock.send(line)
				# sys.stdout.write(sock.recv(1024))
				# sys.stdout.flush()
			# except SSL.Error:
				# print 'Connection died unexpectedly'
				# break
			str=fo.read(1024)
			if str=='':
				break
			try:
				sock.send(str)
				sys.stdout.write(sock.recv(1024))
				sys.stdout.flush()
			except SSL.Error:
				print 'Connection died unexpectedly'
				break
	if opt=='2': ## send file
		fo=open(filename,"rb")
		
		while 1:
			# line = sys.stdin.readline()
			# if line == '':
				# break
			# try:
				# sock.send(line)
				# sys.stdout.write(sock.recv(1024))
				# sys.stdout.flush()
			# except SSL.Error:
				# print 'Connection died unexpectedly'
				# break
			str=fo.read(1024)
			if str=='':
				break
			try:
				sock.send(str)
				sys.stdout.write(sock.recv(1024))
				sys.stdout.flush()
			except SSL.Error:
				print 'Connection died unexpectedly'
				break
				
	print 'Operation succesfully completed'
	cont=raw_input('Press 1 to continue otherwise quit\n')
	if not cont=='1':
		break
		
	
		
fo.close()
sock.shutdown()
sock.close()