# -*- coding: latin-1 -*-
#
# Copyright (C) AB Strakt
# Copyright (C) Jean-Paul Calderone
# See LICENSE for details.

"""
Simple SSL client, using blocking I/O
"""

from OpenSSL import SSL
import sys, os, select, socket

def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print 'Got certificate: %s' % cert.get_subject()
    return ok

if len(sys.argv) < 3:
    print 'Usage: python[2] client.py HOST PORT'
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
ctx = SSL.Context(SSL.TLSv1_METHOD)
ctx.set_verify(SSL.VERIFY_PEER, verify_cb) # Demand a certificate
ctx.use_privatekey_file (os.path.join(dir, 'client.pkey'))
ctx.use_certificate_file(os.path.join(dir, 'client.cert'))
ctx.load_verify_locations(os.path.join(dir, 'CA.cert'))



def dropClient(cli, errors=None):
    if errors:
        print 'Client %s left unexpectedly:' % (clients[cli],)
        print '  ', errors
    else:
        print 'Client %s left politely' % (clients[cli],)
    if not errors:
        cli.shutdown()
    cli.close()

# Set up client
sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
sock.connect((sys.argv[1], int(sys.argv[2])))


while True:
    opt=raw_input("Please select send or receive\n 1 send, 2 receive\n")
	filename=raw_input("Enter your filename\n")
	
	if opt=='1':	## send file
	    fo=open(filename,"rb")
		
	    try:
            sock.send('RECEIVE'+' '+filename+' ')
	    	#sys.stdout.write(sock.recv(20))
	    	#sys.stdout.flush()
	    except SSL.Error:
           	print 'Connection died unexpectedly'
                break
            while 1:
                str = fo.read(1024)
                if str == '':
                    break
                try:
                    sock.send(str)
                    #sys.stdout.write(sock.recv(20))
                    #sys.stdout.flush()
                except SSL.Error:
                    print 'Connection died unexpectedly'
                    break
	elif opt=='2':
	    buffer=''
	    try:
			sock.send('GET'+' '+filename+' ')
	    except SSL.Error:
			print 'Connection died unexpetedly'
			break
	    while True:
			try:
				ret=sock.recv(1024)
			except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                pass
            except SSL.ZeroReturnError:		# reach EOF
				print 'Fetched file successfully'
				filename=raw_input('Please enter the file name you want to save:\n')
				fo=open(filename,'wb')
				fo.write(buffer)
				fo.close()
				break
			else:
				buffer=buffer+ret
	else:
	    print 'Wrong selection\nNo action will be taken\n'
	    pass
	
	cont=raw_input('Press 1+Enter to continue or any other key+Enter to quit:')
	if not cont=='1':
	    break;

sock.shutdown()
sock.close()