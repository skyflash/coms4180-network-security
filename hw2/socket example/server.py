import sys
import socket

c=len(sys.argv);
if c < 2:
	print 'port number must be providede!'
	sys.exit(0)

	
#create a TCP/IP socket
sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)

#bind the socket to the port
hostname=socket.gethostname()
server_address=('',int(sys.argv[1]))	# portno must be int instead of string
print  "connecting to %s port %s" % server_address
sock.bind(server_address)

# listen for a connection
sock.listen(5)

while True:
	# wait for a connection
	print "wait for a connection"
	connection, client_address=sock.accept()
	
	try: 
		print "connection from", client_address
	
	#receive the data in small chunks in retransmit it
		while True:
			data=connection.recv(16)
			print  "received %s" % data
			if data:
				print  "sending data back to the client"
				connection.sendall(data)
			else:
				print  "no more data from %s" % client_address
				break
	except socket.error, (value,message):
		if connection:
			connection.close()
		print "socket receiving error"+message
	
	finally:
		#clean up the connection
		connection.close()