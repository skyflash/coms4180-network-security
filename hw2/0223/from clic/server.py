# Author Name: DI CHEN
# UNI: dc3025
"""
This is the server implemented with TLSv1. The server simply receives the file 
the client sends and stores them locally or transfer the client requests.  There 
is no encryption/decryption operation on the server
"""

from OpenSSL import SSL
import sys, os, select, socket


def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print '\nGot certificate: %s\n' % cert.get_subject()
    return ok

if len(sys.argv) < 2:
    print '\nUsage: python[2] server.py PORT\n'
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
#ctx = SSL.Context(SSL.TLSv1_METHOD)
ctx = SSL.Context(SSL.TLSv1_METHOD)
ctx.set_options(SSL.OP_NO_TLSv1)
ctx.set_verify(SSL.VERIFY_PEER|SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
ctx.use_privatekey_file (os.path.join(dir, 'server.pkey'))
ctx.use_certificate_file(os.path.join(dir, 'server.cert'))
ctx.load_verify_locations(os.path.join(dir, 'CA.cert'))

# Set up server
server = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
server.bind(('', int(sys.argv[1])))
server.listen(3) 
server.setblocking(0)

clients = {}


def dropClient(cli, errors=None):
    if errors:
        print '\nClient %s left unexpectedly:\n' % (clients[cli],)
        print '  ', errors
    else:
        print '\nClient %s left politely\n' % (clients[cli],)
    del clients[cli]
    if not errors:
        cli.shutdown()
    cli.close()

while 1:
    try:
        r,w,_ = select.select([server]+clients.keys(), [], [])
    except:
        break

    for cli in r:
        if cli == server:
            cli,addr = server.accept()
            print '\nConnection from %s\n' % (addr,)
            clients[cli] = addr

        else:
            buffer=''
            i=0
            j=0
            command='RECEIVE'
            while True:
                if command=='RECEIVE':
                    try:
                        ret = cli.recv(1024)
                    except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                        pass
                    except SSL.ZeroReturnError:        # reach EOF
                        fileName=raw_input('\nPlease enter the filename for storage:\n')
                        if fileName=='':    # if user press enter directly use default filename copy_filename
                            fileName='copy_'+userFileName
                        fo=open(fileName,'wb')
                        fo.write(buffer)
                        fo.close()
                        dropClient(cli)
                        break
                    except SSL.Error, errors:
                        try:
                            dropClient(cli, errors)
                        except KeyError:
                            print '\nClient has terminated SSL Connection\n'
                            break
                    else:
                        if i==0:
                            #print 'first time'
                            [command,userFileName,rest]=ret.split(' ',2)
                            print '\nthe command is: '+command
                            print '\nthe user file name is: '+userFileName
                            cli.send('I got your command\n')
                        else:
                            buffer= buffer + ret
                            #print ret
                        i+=1
                elif command=='GET':
                                
                    with open(userFileName,'rb') as fo:
                        while True:
                            str=fo.read(1024)
                            if str=='':
                                break
                            try:
                                cli.send(str)
                            except SSL.Error:
                                print '\nConnection died unexpectedly\n'
                                break
                    dropClient(cli)
                    break


for cli in clients.keys():
    cli.close()
server.close()