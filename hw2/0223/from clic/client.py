# Author Name: DI CHEN
# UNI: dc3025

"""
TLSv1 client, using blocking I/O, 
with option of encrypting sent files 
with AES 128 bit in CBC mode or decrypt
 received file with AES 128 bit in CBC mode
"""

from OpenSSL import SSL
import sys, os, select, socket, random, struct
from Crypto.Cipher import AES

def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print '\nGot certificate: %s\n' % cert.get_subject()
    return ok

def encrypt_file_send(key,sock, in_filename, chunksize=64*1024):
    """ Encrypts a file using AES (CBC mode) with the
        given key.

        key:
            The encryption key - a string that must be
            either 16, 24 or 32 bytes long. Longer keys
            are more secure.

        in_filename:
            Name of the input file

        out_filename:
            If None, '<in_filename>.enc' will be used.

        chunksize:
            Sets the size of the chunk which the function
            uses to read and encrypt the file. Larger chunk
            sizes can be faster for some files and machines.
            chunksize must be divisible by 16.
    """
    

    iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    filesize = os.path.getsize(in_filename)

    with open(in_filename, 'rb') as infile:
        sock.send(struct.pack('<Q', filesize))
        
        sock.send(iv)

        while True:
            chunk = infile.read(chunksize)
            if len(chunk) == 0:
                break
            elif len(chunk) % 16 != 0:
                chunk += ' ' * (16 - len(chunk) % 16)
                #outfile.write(encryptor.encrypt(chunk))    
            sock.send(encryptor.encrypt(chunk))


def decrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    """ Decrypts a file using AES (CBC mode) with the
        given key. Parameters are similar to encrypt_file,
        with one difference: out_filename, if not supplied
        will be in_filename without its last extension
        (i.e. if in_filename is 'aaa.zip.enc' then
        out_filename will be 'aaa.zip')
    """
    if not out_filename:
        out_filename = os.path.splitext(in_filename)[0]

    with open(in_filename, 'rb') as infile:
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        iv = infile.read(16)
        decryptor = AES.new(key, AES.MODE_CBC, iv)

        with open(out_filename, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(chunk))

            outfile.truncate(origsize)


def compare_Text(a_file,b_file):    # compare two file contents 
    with open(a_file,'rb') as a:
        with open(b_file,'rb') as b:
            while True:
                a_str=a.read(1024)
                b_str=b.read(1024)
                if not a_str==b_str:
                    return False
                if a_str=='':
                    break
    return True                

if len(sys.argv) < 3:
    print '\nUsage: python[2] client.py HOST PORT\n'
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
ctx = SSL.Context(SSL.TLSv1_METHOD)
ctx.set_verify(SSL.VERIFY_PEER, verify_cb) # Demand a certificate
ctx.use_privatekey_file (os.path.join(dir, 'client.pkey'))
ctx.use_certificate_file(os.path.join(dir, 'client.cert'))
ctx.load_verify_locations(os.path.join(dir, 'CA.cert'))

def chunks(lst, n):
    #Yield sucessive n-sized chuncks from string lst
    for i in xrange(0,len(lst),n):
        yield lst[i:i+n]    


# Set up client
sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
sock.connect((sys.argv[1], int(sys.argv[2])))


while True:
        opt=raw_input("\nPlease select send or receive\n 1 send, 2 receive\n")
        filename=raw_input("\nEnter your filename\n")
    
        if opt=='1':    ## send file
            fo=open(filename,"rb")
            plaintextBuffer=''    
            try:
                sock.send('RECEIVE'+' '+filename+' ')
                sys.stdout.write(sock.recv(20))
                sys.stdout.flush()
            except SSL.Error:
                print '\nConnection died unexpectedly\n'
                break
            
            while True:
                str = fo.read(1024)
                if str == '':
                    break
                plaintextBuffer+=str
            fo.close()
             
            enc_opt=raw_input('\nPlease select whether you want to encrypt the file transfer or not\n Y+Enter for encrypted, N+Enter for non-encrypted\n')
        
            if (enc_opt=='N' or enc_opt=='n'):    # user chooses to send plaintext directly
                for chunk in chunks(plaintextBuffer,1024):
                    try:
                        sock.send(chunk)
                    except SSL.Error:
                        print '\nConnection died unexpectedly\n'
                        break
                print '\nFile Transferred successfully\n'
            
            elif (enc_opt=='Y' or enc_opt=='y'):    #user chooses to encrypt and send ciphertext
            #encryption part
                while True:        # ask for password
                    key=raw_input('\nPlease enter your 16 word password:\n or press enter to use default password\n')
                    if key=='':
                        key="1234567890abcdef"
                    if len(key)==16:
                        break
                    print '\nPassword length is not 16 words, please re-enter the password\n'
        
                encrypt_file_send(key,sock,filename,1024)
            
        elif opt=='2':    #receive file from server
            buffer=''
            try:
                sock.send('GET'+' '+filename+' ')    # send the operation command with target filename
            except SSL.Error:
                print '\nConnection died unexpetedly\n'
                break
            try:
                ret=sock.recv(20)    # receive the feedback from server
                sys.stdout.write(ret)
                sys.stdout.flush()
            except SSL.Error:
                print '\nSSL Connection Error\n'
            while True:
                try:
                    ret=sock.recv(1024)
                except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                    pass
                except SSL.ZeroReturnError:        # reach EOF
                    print 'Fetched file successfully'
                    filename=raw_input('\nPlease enter the file name you want to save:\n')
                    fo=open(filename,'wb')
                    fo.write(buffer)
                    fo.close()
                    # ask for whether to decrypt the file or not
                    dec_opt=raw_input('\nDo you want to decrypt this file?\n Press Y+Enter to enter password to decrypt, or enter other keys to quit\n')
                    if (dec_opt=='Y' or dec_opt=='y'):
                        while True:
                            key=raw_input('\nPlease enter your key below:\n')
                            #key="1234567890abcdef"
                            if len(key)==16:
                                break
                            print '\nKey length is not 16 words, please re-enter\n'
                        # ask for plaintext file name
                        plaintextFileName=raw_input('\nPlease enter your plaintext filename to store\nYou could press enter to use default name\n')
                        if plaintextFileName=='':
                            plaintextFileName='plaintext_'+filename
                    
                        try:
                            decrypt_file(key,filename,plaintextFileName,1024)
                        except ValueError:
                            print '\nCould not decrypt the file, please check the received file again\n'
                        except:
                            print '\nUnexpected error:',sys.exec_info()[0]
                    
                        # do the plaintext compare here
                        comp_choice=raw_input('\nDo you want to compare the plaintext?\n Press Y+Enter or N+Enter to ignore')
                        if (comp_choice=='Y' or comp_choice=='y'):
                            targetFileName=raw_input('\nPlease enter the target file name to compare\n')
                            try:
                                result=compare_Text(targetFileName,plaintextFileName)
                            except IOError: 
                                print "\nError: File does not appear to exit\n Please re-enter the filename\n"
                            else:
                                if result==True:
                                    print '\nThe plaintext content matches up\n'
                                else:
                                    print '\nThe plaintext content does not match up\n'
                        
                        elif (comp_choice=='N' or comp_choice=='n'):
                            pass
                        else:
                            print '\nWrong selection, no action will be taken\n'
    
                    break
                else:
                    buffer=buffer+ret
        else:
            print '\nWrong selection\nNo action will be taken\n'
            pass
        
        # close the SSL socket
        sock.shutdown()
        sock.close()
    
        cont=raw_input('\nPress 1 to continue otherwise quit:')
        if not cont=='1':
            break;
        
        # reopen the connection
        try:
            sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
            sock.connect((sys.argv[1], int(sys.argv[2]))) 
        except socket.error:
            print '\nsocket layer error has occured\n'
            break
        except SSL.Error:
            print '\nSSL Connection has broken\n'
            break
        except:
            print '\nUnexpected error:',sys.exec_info()[0]
            break
