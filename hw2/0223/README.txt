This version has achieved following function:


1. Run the server first
2. Run the client, user will be asked to select either 1 or 2
3. Select 1 will send some file to server. User will the be asked for the filename (no error handling set up yet). User will be asked to whether encrypt
it or not. After that the file will either be sent in plaintext or ciphertext. The client will ask for to continue or not (if continue SSL still won't 
be set up)
4. Server will receive the data from client, when the receiving has finished, user will be asked to input the filename to save. User could press enter to 
ignore this step and then the file will be saved as copy_originalfilename. The server will then close the SSL connection.
5. Select 2 will get some file from server. User will be asked for the filename to input. The server will transfer the file name correspondingly (not error
 handling such as wrong input filename implemented) and shutdown the SSL when transfer is finished. Then user will be asked to provide the name to store locally. After this user will be asked whether
 he wants to decrypt it or not. If yes, password will be asked (no error handling for decryption error), the filename of plaintext to store will be asked (user
could press enter directly and the plaintext will be stored as plaintext_inputfilename). The client will finish the process and ask user whether to quit or noot.



Things left to do:
1. reconnect SSL for if user wants to continue		done
2. filename input error not implemented
3. decryption error not implemented		done
4. if client meets error and quit when SSL has been established, the server will be quit as well. Avoid this.
