/*
    Name: Di Chen
	UNI: dc3025
*/
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#define AES_BITS 128
#define FILE_SIZE (1024*1024)
#define AES_MAX_WIDTH (102
#define BYTE_SIZE 2048


int padding = RSA_PKCS1_PADDING;
 
RSA * createRSA(unsigned char * key,int public)
{
    RSA *rsa= NULL;
    BIO *keybio ;
    keybio = BIO_new_mem_buf(key, -1);
    if (keybio==NULL)
    {
        printf( "Failed to create key BIO");
        return 0;
    }
    if(public)
    {
        rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa,NULL, NULL);
    }
    else
    {
        rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa,NULL, NULL);
    }
    if(rsa == NULL)
    {
        printf( "Failed to create RSA");
    }
 
    return rsa;
}
 
int public_encrypt(unsigned char * data,int data_len,unsigned char * key, unsigned char *encrypted)
{
    RSA * rsa = createRSA(key,1);
    int result = RSA_public_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}
int private_decrypt(unsigned char * enc_data,int data_len,unsigned char * key, unsigned char *decrypted)
{
    RSA * rsa = createRSA(key,0);
    int  result = RSA_private_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
 
 
int private_encrypt(unsigned char * data,int data_len,unsigned char * key, unsigned char *encrypted)
{
    RSA * rsa = createRSA(key,0);
    int result = RSA_private_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}
int public_decrypt(unsigned char * enc_data,int data_len,unsigned char * key, unsigned char *decrypted)
{
    RSA * rsa = createRSA(key,1);
    int  result = RSA_public_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
 
void printLastError(char *msg)
{
    char * err = malloc(130);;
    ERR_load_crypto_strings();
    ERR_error_string(ERR_get_error(), err);
    printf("%s ERROR: %s\n",msg, err);
    free(err);
}


int main(int argc , char *argv[])
{
	int i;
    int sock, portno,n, keep_write,  sent_bytes;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char message[1000] , server_reply[2000];
    //unsigned char buffer[(1024*1024)]; //used as buffer to store the content of read in file
    unsigned char buf[256];  //used to store the confirmed message from server
    //FILE *fp;
     
	unsigned char passwd[30];
	int passwd_len;
	unsigned char encrypted_passwd[4096];
	unsigned char decrypted_passwd[4096];
	
	char filepath[100];
	int repeat_enter_passwd=0;

	FILE *fp, *pub_key, *pri_key;
	unsigned char text[FILE_SIZE];
	int sz; //read in file size
	
	unsigned char *ciphertext = NULL;
	unsigned char *plaintext = NULL;  // plaintext is used to store decrypted plaintext
	unsigned char *passkey, *passiv, *plaintxt;

    	unsigned char out[FILE_SIZE]; 
	unsigned char decout[FILE_SIZE];
	//int aes_block;  //how many blocks of aes should be done 
	const EVP_CIPHER *cipher_type;
	EVP_CIPHER_CTX en;
  	EVP_CIPHER_CTX de;
  	EVP_CIPHER_CTX_init(&en);
  	EVP_CIPHER_CTX_init(&de);
 	AES_KEY wctx;
	int input_len; // plaintext file length to be encrypted
	int hash_len;  // plaintext file length to be hashed
	unsigned char iv[AES_BLOCK_SIZE]; 
	static const int MAX_PADDING_LEN = 16;
	int bytes_written = 0;
    int ciphertext_len = 0;

	
	SHA256_CTX sha256;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	int hashed_len;
	unsigned char encrypted_hash[4096];
	unsigned char decrypted_hash[4096];
	int bad_hash_encrypt=0;
  	
	
	int encrypted_len, decrypted_len;	// used for RSA of password
	int encrypted_len_hash, decrypted_len_hash; // used for RSA of sha256 hashed signature

	/* default RSA keys */
	unsigned char publicKey_default[]="-----BEGIN PUBLIC KEY-----\n"\
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"\
"ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"\
"vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"\
"fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"\
"i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"\
"PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"\
"wQIDAQAB\n"\
"-----END PUBLIC KEY-----\n";
	unsigned char publicKey[4096];	// RSA public key buffer
  
	unsigned char privateKey_default[]="-----BEGIN RSA PRIVATE KEY-----\n"\
"MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"\
"vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"\
"Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"\
"yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"\
"WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"\
"gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"\
"omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"\
"N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"\
"X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"\
"gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"\
"vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"\
"1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"\
"m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"\
"uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"\
"JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"\
"4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"\
"WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"\
"nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"\
"PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"\
"SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"\
"I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"\
"ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"\
"yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"\
"w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"\
"uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"\
"-----END RSA PRIVATE KEY-----\n";
	unsigned char privateKey[4096];		// RSA private key buffer

	// get password and check invalidty
	int passwd_wrong_counter=0;
  	do{
    	repeat_enter_passwd=0;
   		printf("Enter a password :\n");
    	gets(passwd);
    	if(strlen(passwd)!=16){
		printf("Password length must be 16, please re-enter \n");
		repeat_enter_passwd=1;
    	}
    	else{
	   		for(i=0;passwd[i]!=0;i++){
	   			if((passwd[i]<33&&passwd[i]>0) || passwd[i]>125){
					printf("Contain illegal characters, please re-enter password: \n");
					repeat_enter_passwd=1;
					break;
	      		}
  	    	}    
	     	
    		}	
    		printf("The input password is %s\n",passwd);  // print password content
   	
   

    	if(repeat_enter_passwd) {
		//free(passwd);
        	memset(passwd,0,sizeof(passwd));
			passwd_wrong_counter++;
    	}
		if(passwd_wrong_counter>3){
			puts("password entered wrong more than 3 times");
			puts("exiting the program...");
			return 0;
		}
    
  	}while(repeat_enter_passwd);

	        
	// get plaintext file path and file content
	printf("Please enter your file path below\n");
	gets(filepath);		//
	
	fp=fopen(filepath,"r");

	int wrong_file_counter=0;
	while(fp==NULL)
    	{
        	puts("\n fopen() Error!!!");
			puts("Please reenter the file path");
			memset(filepath,0,sizeof(filepath));
			gets(filepath);
			wrong_file_counter++;
			if(wrong_file_counter>3)
			{
					puts("More than 3 times wrong input, exitting the program...");  //allow no more than 3 times of wrong input
					return 0;
			}
			fp=fopen(filepath,"r");
    }
	fseek(fp, 0L, SEEK_END);
	sz = ftell(fp);
	fseek(fp,0,SEEK_SET);
	fread(text,sz,1,fp);
	fclose(fp); //close the plaintext pointer

	
	// get the RSA public key file path
	memset(filepath,0,sizeof(filepath));
	puts("Pleae enter the RSA public key file path and name");
	puts("if press enter directly, the program will use default AES public key value");
	gets(filepath);
	if(!strcmp(filepath,"\0")){
			puts("Default public key value selected");
			printf("\n");
			memcpy(publicKey,publicKey_default,sizeof(publicKey_default));
	}
	else{
			if((pub_key=fopen(filepath,"r"))!=NULL)
			{
				//publicKey_rsa=PEM_read_RSA_PUBKEY(pub_key,NULL,NULL,NULL);
				if(fread(publicKey,4096,1,pub_key))
				//if(publicKey_rsa==NULL)
				{
					printf("\n\tCould NOT read RSA public key file\n");
					fclose(pub_key);
				}
				else
				{
					puts("Read Public Key Success");
					fclose(pub_key);
					//puts(publicKey);
					
				}
			}
			else{
				puts("Could not open public key file");
				return 1;
			}

	}	
	
	// get the AES private key file path
	memset(filepath,0,sizeof(filepath));
	puts("Pleae enter the RSAprivate key file path and name");
	puts("if press enter directly, the program will use default AES private key value");
	gets(filepath);
	if(!strcmp(filepath,"\0")){
			puts("Default private key value selected");
			printf("\n");
			memcpy(privateKey,privateKey_default,sizeof(privateKey_default));
	}
	else{
			
			if((pri_key=fopen(filepath,"r"))!=NULL){
				//privateKey_rsa=PEM_read_RSAPrivateKey(pri_key,NULL,NULL,NULL);
				if(fread(privateKey,4096,1,pri_key))
				//if(privateKey_rsa==NULL)
				{
					perror("\n\tCould NOT read RSA public key file\n");
					fclose(pri_key);
				}
				else
				{
					puts("Read Private Key success");
					fclose(pri_key);
					//puts(privateKey);
				}
			}
			else{
				puts("Could not open public key file");
				return 1;
			}
	}	

	for(i=0; i<AES_BLOCK_SIZE; ++i) 
    		iv[i]=0;
	
	cipher_type = EVP_aes_128_cbc();
	EVP_EncryptInit_ex(&en, cipher_type, NULL, passwd, iv);
    EVP_DecryptInit_ex(&de, cipher_type, NULL, passwd, iv);


	// We add 1 because we're encrypting a string, which has a NULL terminator and want that NULL terminator to be present when we decrypt.
	input_len=strlen(text)+1;
	printf("the plaintext length is : %d\n", input_len);

	
    ciphertext = (unsigned char *) malloc(input_len + MAX_PADDING_LEN);


        /* allows reusing of 'e' for multiple encryption cycles */
    if(!EVP_EncryptInit_ex(&en, NULL, NULL, NULL, NULL)){
        printf("ERROR in EVP_EncryptInit_ex \n");
        return 1;
    }

      	// starting encryption
      	
      	if(!EVP_EncryptUpdate(&en,
                           ciphertext, &bytes_written,
                           (unsigned char *) text, input_len) ) {
        	printf("ERROR in EVP_EncryptUpdate \n");
        	return 1;
      	}
      	ciphertext_len += bytes_written;

	// EVP_EncryptFinal_ex writes the padding. 
      	if(!EVP_EncryptFinal_ex(&en,
                              ciphertext + bytes_written,
                              &bytes_written)){
       	 	printf("ERROR in EVP_EncryptFinal_ex \n");
        	return 1;
      	}
      	ciphertext_len += bytes_written;

      	EVP_CIPHER_CTX_cleanup(&en);


      	printf("Input len: %d, ciphertext_len: %d\n", input_len, ciphertext_len);
		puts("The first 32 char of ciphertext");
		for(i=0;i<32;i++){
			printf("%x ",*(ciphertext+i));
		}	
		puts("");

	
		
	/* AES decryption part */
    	// malloc plaintext
      	plaintext = (unsigned char *) malloc(ciphertext_len);

      	if(!EVP_DecryptInit_ex(&de, NULL, NULL, NULL, NULL)){
        	printf("ERROR in EVP_DecryptInit_ex \n");
        	return 1;
      	}

      
      	int plaintext_len = 0;
      	if(!EVP_DecryptUpdate(&de,
                            plaintext, &bytes_written,
                            ciphertext, ciphertext_len)){
        	printf("ERROR in EVP_DecryptUpdate\n");
        	return 1;
      	}
      	plaintext_len += bytes_written;

      	// This function verifies the padding and then discards it.  
      	if(!EVP_DecryptFinal_ex(&de,
                              plaintext + bytes_written, &bytes_written)){
        	printf("ERROR in EVP_DecryptFinal_ex\n");
        	return 1;
      	}
      	plaintext_len += bytes_written;

      	EVP_CIPHER_CTX_cleanup(&de);

      	if( strcmp(text, (char *) plaintext) == 0 ) {
          	printf("Decrypted data matches input data.\n");
      	}
      	else {
          	printf("Decrypted data does not match input data.\n");
      	}

   	
	/* sha-256 part*/

	memset(hash,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	memset(decrypted_hash,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	hash_len=strlen(text);  // do not include the '\0' here, or +1, otherwise the hash value would be wrong
	printf("hash_len is : %d ",hash_len);
	printf("plaintext_len is :%d\n", plaintext_len);
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, text,hash_len);
	SHA256_Final(hash,&sha256);

	
	printf("output SHA256 hash value:\n");
	for (i=0; i<SHA256_DIGEST_LENGTH;i++)
		printf("%x", hash[i]);
	printf("\n");
	
	/*RSA encrypt of password using public key*/
	passwd_len=strlen(passwd);
	encrypted_len= public_encrypt(passwd,passwd_len,publicKey,encrypted_passwd);
	if(encrypted_len == -1)
	{
    		printLastError("Public Encrypt failed ");
    		exit(0);
	}
	printf("Encrypted length =%d\n",encrypted_len);
 
	decrypted_len = private_decrypt(encrypted_passwd,encrypted_len,privateKey, decrypted_passwd);
	if(decrypted_len == -1)
	{
    		printLastError("Private Decrypt failed ");
    		exit(0);
	}
	if(strcmp(decrypted_passwd,passwd)==0)
		printf("encryption/decryption of password matches up\n");
	else
		printf("encryption/decryption of password does not match up\n");
	printf("Decrypted Text =%s\n",decrypted_passwd);
	printf("Decrypted Length =%d\n",decrypted_len);

	printf("\n");
 	

	
 	/* RSA encrypt of hashed value using private key */
	
	encrypted_len_hash= private_encrypt(hash,SHA256_DIGEST_LENGTH,privateKey,encrypted_hash); // when encrypt the hash signature always use full array size just in case '\0' might appear in the middle
	if(encrypted_len_hash == -1)
	{
	    printLastError("Private Encrypt of SHA256 Signature Failed");
	    exit(0);
	}
	printf("Encrypted length =%d\n",encrypted_len_hash);
 	
	
	/*check decrypted SHA256 signature on local machine*/
	decrypted_len_hash= public_decrypt(encrypted_hash,encrypted_len_hash,publicKey, decrypted_hash);
	if(decrypted_len_hash == -1)
	{
    		printLastError("Public Decrypt of SHA256 Signature Failed");
    		exit(0);
	}
	printf("Orignal SHA256 Signature:");
	for (i=0; i<SHA256_DIGEST_LENGTH;i++)
		
		printf("%x", hash[i]);
		
	printf("\n");
	printf("Decrypted SHA256 Signature:");
	for (i=0; i<SHA256_DIGEST_LENGTH;i++)
		
		printf("%x", decrypted_hash[i]);
	printf("\n");
	printf("Decrypted Length =%d\n",decrypted_len_hash);

	for(i=0;i<SHA256_DIGEST_LENGTH;i++){
		if(decrypted_hash[i]!=hash[i]){
			bad_hash_encrypt=1;
			break;
		}
	}
	if(!bad_hash_encrypt)
		printf("encryption/decryption of SHA256 Signature matches up\n");
	else
		printf("encryption/decryption of SHA256 Signature does not match up\n"); 
	 
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock<0)
    {
        printf("Could not create socket");
		exit(1);
    }
    puts("Socket created");
     
    serv_addr.sin_addr.s_addr =  INADDR_ANY;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons( portno);
    
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
 
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&serv_addr , sizeof(serv_addr)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
     
    puts("Connected\n");

	// send the ciphertext
	sent_bytes=0;
	keep_write=1;
	//int ciphertext_len_sent=input_len + MAX_PADDING_LEN-1;
	int ciphertext_len_sent=ciphertext_len;
	while(keep_write){
		if(sent_bytes<ciphertext_len_sent-BYTE_SIZE)		 // control the outputlength
			n=write(sock,ciphertext+sent_bytes,BYTE_SIZE);
		else{
			n=write(sock,ciphertext+sent_bytes,ciphertext_len_sent-sent_bytes);	
			keep_write=0;
		}
	
		if (n < 0) 
			error("ERROR writing to socket");
		sent_bytes+=n;
		n = read(sock,buf,255);
    		if (n < 0) 
        		error("ERROR reading from socket");
	}
	printf("sent bytes: %d\n",sent_bytes);
	printf("sending ciphertext success\n");
	printf("\n");
	
	
	// send the SHA256 Signature
    printf("Sending the SHA256 Signature Now\n");
    n = write(sock,encrypted_hash,1024);
	if (n < 0) 
        error("ERROR writing SHA256 Signature to socket");
	else
		printf("SHA 256 Signature sent\n");
    bzero(buf,256);
    n = read(sock,buf,255);
    if (n < 0) 
        error("ERROR reading from socket");
    printf("%s\n",buf);
	
	// send the RSA encrypted password
	printf("Sending the RSA encrypted password now\n");
	n= write(sock, encrypted_passwd, 1024);
	
	if (n < 0)	error("ERROR writing RSA encrypted password to socket");
	else	printf("RSA encrypted password sent\n");
	bzero(buf,256);
    n = read(sock,buf,255);
    if (n < 0) 
        error("ERROR reading from socket");
    printf("%s\n",buf);
	
	  
	 
	 
	free(ciphertext);
   	free(plaintext); 
    close(sock);
    return 0;
}