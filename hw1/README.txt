1. The generation of RSA keys

The two RSA keys have been generated using openssl package in the bash shell

first:
openssl genrsa -out private.pem 2048
then:
openssl rsa -in private.pem -outform PEM -pubout -out public.pem

2. necessary files:
server.c, client1.c, client2.c are codes to be compiled.
Makefile script is for compiling these codes.
private.pem and public.pem is the provided RSA key file. 
It is required that all files are placed under same directory to guarantee runtime success.


3. The running flow of the program

1. type "make" in the directory where all files are placed and server.out, client1.out, client2.out should be compiled.
2. type "make clean" to remove all compiled files.
3. First execute the server.out file like "./server.out portnumber", server will be started and waiting for clients to connect.
4. Then type in "./client1.out hostname portnumber" to launch client1. You will be first asked to input 16 character as password for AES encryption, if illeagal characters appeared or input character length does not match 16,
user will be asked to input again. Total of 5 chances will be given otherwise program will exit.
5. Then user will be asked to type in the target plaintext file to be encrypted and transferred. File path could be either the full path or the relevant path to the current directory. 
The default directory is the local directory. If file does not exist, user will be asked to input again. Total of 5 chances will be allowed to input correct file path.
6. User then will be asked to select RSA public key file. The file path requirement is the same as target plaintext file. Total of 5 chances will be allowed to find correct file.
If user press Enter directly, the program will use the default RSA public and private key hard coded inside the program. Notice that if default keys must be both selected for private and public otherwise encryption would fail.
7. Once these selection are finished, client1 encrypt all the files and signatures, set up the socket connection to server, and forward all of them to server. As sort of sanity check, the author kept the 
self-decryption of ciphertext and signature as well prior to forwarding through socket to server. These results will be printed on screen. Once the transfer is done, client1 will close the socket and exit.
Meanwhile the server will receive all data from client1, and it will wait for client2 connection. 
8. Then type in "./client2.out hostname portnumber" to launch client2. At the beginning of client2 user will be asked for RSA key files as well. Same rules apply as in previous cases for client1. Also press Enter directly will select
hard coded default key values.
9. At this time file transfer will not start immediately from server to client2. On the server side user will be asked to select either trust mode (t) or untrusted mode (u).
User will not allow to make any other choice. If "t" is selected, server will not ask for anything else and all the received data from client1 will be directly forwared to client2.
If "u" is selected, server will ask for password used for AES encryption, if the input password does not match up with the password in client1, during the decryption in client2 it will fail and give "ciphertext has been changed" warning.
Then user will be asked for plaintext file to encrypt. The path name could be either full path or relevant path to current directory. The default is local directory. When file is selected, the server will do the encryption and send the 
newly encrypted ciphertext and received signature, encrypted password to client2. Then server will exit.
10. When the client2 receives the data, it will do the decryption and verifies the signature. If the ciphertext cannot be decrypted appropriately because user input the wrong password in server "u" mode, it will complain and quit. If
the file plaintext has been changed during the transfer, it will not pass the signature verification and the screen would tell "SHA256 signature does not match up" and exit. Only the correct ciphertext received and SHA256 verification passed
will success and show "SHA256 signature matches up" and the plaintext will be saved to client2data.txt in the local directory. Then the client2 will finish.


Some notices:
1. If client2's socket does not receive server's response more than 40 seconds. It will close the socket and quit. 
2. If server is accidentally terminated such as user made too many wrong selections during the "u" or "t" selection process, client2 will terminate as well.
3. If given RSA key pairs does not fit each other, the encryption or decryption wil fail with error message printed on screen and program exit.
