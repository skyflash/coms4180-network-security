/*
    Name: Di Chen
	UNI: dc3025
*/
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#define AES_BITS 128
#define FILE_SIZE (1024*1024)
#define AES_MAX_WIDTH (102
#define BYTE_SIZE 2048


int padding = RSA_PKCS1_PADDING;
 
RSA * createRSA(unsigned char * key,int public)
{
    RSA *rsa= NULL;
    BIO *keybio ;
    keybio = BIO_new_mem_buf(key, -1);
    if (keybio==NULL)
    {
        printf( "Failed to create key BIO");
        return 0;
    }
    if(public)
    {
        rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa,NULL, NULL);
    }
    else
    {
        rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa,NULL, NULL);
    }
    if(rsa == NULL)
    {
        printf( "Failed to create RSA");
    }
 
    return rsa;
}
 
int public_encrypt(unsigned char * data,int data_len,unsigned char * key, unsigned char *encrypted)
{
    RSA * rsa = createRSA(key,1);
    int result = RSA_public_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}
int private_decrypt(unsigned char * enc_data,int data_len,unsigned char * key, unsigned char *decrypted)
{
    RSA * rsa = createRSA(key,0);
    int  result = RSA_private_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
 
 
int private_encrypt(unsigned char * data,int data_len,unsigned char * key, unsigned char *encrypted)
{
    RSA * rsa = createRSA(key,0);
    int result = RSA_private_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}
int public_decrypt(unsigned char * enc_data,int data_len,unsigned char * key, unsigned char *decrypted)
{
    RSA * rsa = createRSA(key,1);
    int  result = RSA_public_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
 
void printLastError(char *msg)
{
    char * err = malloc(130);;
    ERR_load_crypto_strings();
    ERR_error_string(ERR_get_error(), err);
    printf("%s ERROR: %s\n",msg, err);
    free(err);
}


int main(int argc , char *argv[])
{
	int i;
	int error=0; //use to check socket status
	socklen_t len = sizeof (error);
	int retval;


	struct timeval timeout;      // timeval struct used to set the timeout parameter for client2 socket
   	timeout.tv_sec = 40;		// client2 will automatically shutdown if server does not reply more than 40 seconds
   	timeout.tv_usec = 0;

    int sock, portno,n, keep_write,  sent_bytes, read_size;
   	struct sockaddr_in serv_addr;
   	struct hostent *server;
   	char message[1000] , server_reply[2000];
    unsigned char buf[256];  //used to store the confirmed message from server
    
     	 
	unsigned char real_ciphertext[FILE_SIZE];
	unsigned char passwd[30];
	int passwd_len;
	unsigned char encrypted_passwd[4096];
	unsigned char decrypted_passwd[4096];
	unsigned char encrypted_hash_receiver[4096];  //used to receiver RSA encrypted SHA256 Signature
	unsigned char encrypted_passwd_receiver[4096];
	unsigned char buffer[BYTE_SIZE];
	unsigned char ciphtertext_receiver[1024*1024];
	char filepath[100];
	int repeat_enter_passwd=0;
	int ciphertext_len=0;
	int plaintext_len = 0;

	FILE *fp, *fc,*fw;
	unsigned char text[FILE_SIZE];
	int sz; //read in file size
	
 	//unsigned char text[]="virident aesfghk arident aesfghk";  //initial manually set plaintext used for texting aes
	unsigned char *ciphertext = NULL;
	unsigned char *plaintext = NULL;  // plaintext is used to store decrypted plaintext
	unsigned char *passkey, *passiv, *plaintxt;

    	unsigned char out[FILE_SIZE]; 
	unsigned char decout[FILE_SIZE];
	//int aes_block;  //how many blocks of aes should be done 
	const EVP_CIPHER *cipher_type;
	EVP_CIPHER_CTX en;
  	EVP_CIPHER_CTX de;
  	EVP_CIPHER_CTX_init(&en);
  	EVP_CIPHER_CTX_init(&de);
 	AES_KEY wctx;
	int input_len; // plaintext file length to be encrypted
	int hash_len;  // plaintext file length to be hashed
	unsigned char iv[AES_BLOCK_SIZE]; 
	static const int MAX_PADDING_LEN = 16;
	int bytes_written=0;

	
	SHA256_CTX sha256;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	int hashed_len;
	unsigned char encrypted_hash[4096];
	unsigned char decrypted_hash[4096];
	unsigned char ciphertext_receiver[FILE_SIZE];
	int received_bytes=0;
	int bad_hash_encrypt=0;
  	
	
	int encrypted_len, decrypted_len;	// used for RSA of password
	int encrypted_len_hash, decrypted_len_hash; // used for RSA of sha256 hashed signature

	
	/*default RSA keys*/
	unsigned char publicKey_default[]="-----BEGIN PUBLIC KEY-----\n"\
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"\
"ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"\
"vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"\
"fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"\
"i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"\
"PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"\
"wQIDAQAB\n"\
"-----END PUBLIC KEY-----\n";
	unsigned char publicKey[4096];		// RSA public key buffer
  
	unsigned char privateKey_default[]="-----BEGIN RSA PRIVATE KEY-----\n"\
"MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"\
"vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"\
"Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"\
"yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"\
"WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"\
"gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"\
"omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"\
"N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"\
"X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"\
"gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"\
"vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"\
"1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"\
"m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"\
"uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"\
"JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"\
"4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"\
"WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"\
"nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"\
"PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"\
"SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"\
"I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"\
"ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"\
"yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"\
"w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"\
"uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"\
"-----END RSA PRIVATE KEY-----\n";
	unsigned char privateKey[4096];		// RSA private Key buffer
	FILE *pub_key, *pri_key;
	
	
	
		// get the AES public key file path
	memset(filepath,0,sizeof(filepath));
	puts("Pleae enter the AES public key file path and name");
	puts("if press enter directly, the program will use default AES public key value");
	gets(filepath);
	if(!strcmp(filepath,"\0")){
			puts("Default public key value selected");
			printf("\n");
			memcpy(publicKey,publicKey_default,sizeof(publicKey_default));
	}
	else{
			if((pub_key=fopen(filepath,"r"))!=NULL)
			{
				//publicKey_rsa=PEM_read_RSA_PUBKEY(pub_key,NULL,NULL,NULL);
				if(fread(publicKey,4096,1,pub_key))
				//if(publicKey_rsa==NULL)
				{
					printf("\n\tCould NOT read RSA public key file\n");
					fclose(pub_key);
				}
				else
				{
					puts("Read Public Key Success");
					fclose(pub_key);
					//puts(publicKey);
					
				}
			}
			else{
				puts("Could not open public key file");
				return 1;
			}

	}	
	
	// get the AES private key file path
	memset(filepath,0,sizeof(filepath));
	puts("Pleae enter the AES private key file path and name");
	puts("if press enter directly, the program will use default AES private key value");
	gets(filepath);
	if(!strcmp(filepath,"\0")){
			puts("Default private key value selected");
			printf("\n");
			memcpy(privateKey,privateKey_default,sizeof(privateKey_default));
	}
	else{
			
			if((pri_key=fopen(filepath,"r"))!=NULL){
				//privateKey_rsa=PEM_read_RSAPrivateKey(pri_key,NULL,NULL,NULL);
				if(fread(privateKey,4096,1,pri_key))
				//if(privateKey_rsa==NULL)
				{
					perror("\n\tCould NOT read RSA public key file\n");
					fclose(pri_key);
				}
				else
				{
					puts("Read Private Key success");
					fclose(pri_key);
					//puts(privateKey);
				}
			}
			else{
				puts("Could not open public key file");
				return 1;
			}
	}	
	

	
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock<0)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    serv_addr.sin_addr.s_addr =  INADDR_ANY;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons( portno);
    
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
 
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&serv_addr , sizeof(serv_addr)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
     
    puts("Connected\n");

	
	bzero(buffer,BYTE_SIZE);
	bzero(ciphtertext_receiver,FILE_SIZE);
	// receiver ciphtertext from server

	if (setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        	perror("setsockopt failed\n");




	while(1) 
    {
        read_size = read(sock , buffer , BYTE_SIZE);
		//Send the message back to client
		if(read_size<0){
			puts("Server not responding for too long, exiting...");	//if server does not send any message to client2 or the socke hangs up till 40 seconds, client2 process will quit
			close(sock);
			return 1;			
		}
		memcpy(ciphertext_receiver+received_bytes,buffer,read_size);
		received_bytes+=read_size;
		if(read_size==0){
			if(received_bytes==0){
				puts("Server has terminated file transfer");	// if server process is killed before transfer then client will close as well
				close(sock);
				return 0;
			}
			puts("reading ciphertext finished");
			break;
		}
		if(read_size==BYTE_SIZE)
			bzero(buffer,BYTE_SIZE)	;
	}	
	close(sock);
	puts("Server disconnected");
	printf("Total of %d bytes received\n",received_bytes);
	//printf("The ciphertext_receiver size is %d\n",(int)strlen(ciphertext_receiver));
	
	
	for (i=0;i<1024;i++)
	{
		encrypted_hash_receiver[i]=ciphertext_receiver[i+received_bytes-1024*2];
		encrypted_passwd_receiver[i]=ciphertext_receiver[i+received_bytes-1024];
	}
	ciphertext_len=received_bytes-1024*2;
	
    //this part below is to check the decrypted sha256 signature
	encrypted_len_hash=256;
	decrypted_len_hash= public_decrypt(encrypted_hash_receiver,encrypted_len_hash,publicKey, decrypted_hash);
	if(decrypted_len_hash == -1)
	{
    		printLastError("Public Decrypt of SHA256 Signature Failed, the ciphertext is broken");
    		exit(0);
	}
	printf("Decrypted SHA256 Signature:");
	for ( i=0; i<SHA256_DIGEST_LENGTH;i++)
		
		printf("%x", decrypted_hash[i]);
	printf("\n");
	printf("Decrypted Length =%d\n",decrypted_len_hash);

	// this part below is to check the decrypted password for AES
	
	encrypted_len=256;
	decrypted_len = private_decrypt(encrypted_passwd_receiver,encrypted_len,privateKey, decrypted_passwd);
	if(decrypted_len == -1)
	{
    		printLastError("Private Decrypt of AES password failed ");
    		exit(0);
	}
	printf("Decrypted Text =%s\n",decrypted_passwd);
	printf("Decrypted Length =%d\n",decrypted_len);
	printf("\n");
     
	// this part below is to AES decrypt the ciphertext
	memset(iv,0,AES_BLOCK_SIZE); //set iv to 0	
	cipher_type = EVP_aes_128_cbc();
	EVP_DecryptInit_ex(&de, cipher_type, NULL, decrypted_passwd, iv);
	
  	plaintext = (unsigned char *) malloc(ciphertext_len);		// malloc plaintext
	ciphertext=(unsigned char*) malloc(ciphertext_len);		//malloc ciphertext buffer
	memset(ciphertext,0,ciphertext_len+1);
	memcpy(ciphertext,ciphertext_receiver,ciphertext_len);	//copy pure ciphertext content to its buffer
	
	puts("Starting decrypting ciphtertext");
	printf("The ciphertext length is : %d\n", ciphertext_len);
	
	if(!EVP_DecryptInit_ex(&de, NULL, NULL, NULL, NULL)){
		printf("ERROR in EVP_DecryptInit_ex \n");
        return 1;
    }
          
    if(!EVP_DecryptUpdate(&de,
            plaintext, &bytes_written,
            ciphertext, ciphertext_len)){
        printf("ERROR in EVP_DecryptUpdate\n");
        return 1;
    }
    plaintext_len += bytes_written;

	printf("The plaintext length before final is %d\n",plaintext_len);
    // This function verifies the padding and then discards it.  
    if(!EVP_DecryptFinal_ex(&de,
                              plaintext + bytes_written, &bytes_written)){
			puts("The AES decryption failed, ciphertext has been changed during transmission!");
			free(ciphertext);
			free(plaintext);
        		return 1;
    }
    plaintext_len += bytes_written;

    EVP_CIPHER_CTX_cleanup(&de);
	
	printf("The ciphertext length is %d, the plaintext length is %d\n",ciphertext_len,plaintext_len);

    // this part below is to compute the received plaintext SHA256 signature and compare with received signature
	memset(hash,0,SHA256_DIGEST_LENGTH*sizeof(unsigned char));
	hash_len=strlen(plaintext); 
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, plaintext,hash_len);
	SHA256_Final(hash,&sha256);

	
	printf("output SHA256 hash value:\n");
	for (i=0; i<SHA256_DIGEST_LENGTH;i++)
		printf("%x", hash[i]);
	printf("\n");
	
	for(i=0;i<SHA256_DIGEST_LENGTH;i++){
		if(decrypted_hash[i]!=hash[i]){
			bad_hash_encrypt=1;
			break;
		}
	}

	if(!bad_hash_encrypt)
		printf("encryption/decryption of SHA256 Signature matches up\n");
	else
		printf("encryption/decryption of SHA256 Signature does not match up\nThe ciphertext has been changed during transmission\n");
			
	if(!bad_hash_encrypt){
		puts("Encryption/decryption of SHA256 Signature matches up");
		puts("Signature authentication passed, saving the plaintext...");
		fw=fopen("./client2data.txt","w");
		if(fw==NULL)
		{
			printf("\n fopen() Error!!!\n");
			return 1;
		}
		
		if(!fwrite(plaintext,1,plaintext_len-1,	fw))
			perror("Saving client2data failed");
		fclose(fw); //close file pointer to client2data
		free(ciphertext);
   		free(plaintext); 
	}
	else{
		free(ciphertext);
   		free(plaintext); 
		puts("Encryption/decryption of SHA256 Signature does not match up"); 
		puts("Exiting the program...");
		return 0;
	}   
    
    return 0;
}