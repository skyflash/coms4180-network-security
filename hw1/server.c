/* Name: Di Chen
	UNI: dc3025
*/


#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread
#include <sys/types.h> 
#include <netinet/in.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/err.h>

#define BYTE_SIZE 2048
#define AES_BITS 128
#define FILE_SIZE (1024*1024)
#define AES_MAX_WIDTH (102)

unsigned char buffer[BYTE_SIZE];
unsigned char client2_buffer[BYTE_SIZE];
unsigned char ciphertext_receiver[1024*1024];

volatile int running_threads=0;
//the thread function
void client1_handler(void *);
void client2_handler(void *);

int padding = RSA_PKCS1_PADDING;
int received_bytes=0;
int ciphertext_len_local=0;
int ciphertext_len=0;

char filepath[100];
unsigned char passwd[30];


//////////
char choice;  //choose which mode to operate, t trust, u untrusted
int passwd_wrong_counter=0;
int repeat_enter_passwd=0;
const EVP_CIPHER *cipher_type;
EVP_CIPHER_CTX en;
AES_KEY wctx;
int input_len; // plaintext file length to be encrypted
unsigned char iv[AES_BLOCK_SIZE]; 
static const int MAX_PADDING_LEN = 16;
int bytes_written = 0;
	
	
//used to preparing encryption of random file 
FILE *fp; //pointer to the random file
unsigned char text[FILE_SIZE];  //random file plaintext buffer
int sz; //read in random file file size
unsigned char *ciphertext = NULL; // random file ciphertext buffer

 
// encryption part declaration
SHA256_CTX sha256;
unsigned char hash[SHA256_DIGEST_LENGTH];
int hashed_len;
unsigned char encrypted_hash[4096];
unsigned char decrypted_hash[4096];
unsigned char decrypted_passwd[4096];
unsigned char encrypted_passwd_receiver[1024]; 
unsigned char encrypted_hash_receiver[1024];  //used to receiver RSA encrypted SHA256 Signature
unsigned char encrypted_passwd_receiver[1024]; 
	
int encrypted_len, decrypted_len;	// used for RSA of password
int encrypted_len_hash, decrypted_len_hash; // used for RSA of sha256 hashed signature

void error(char *msg)
{
    perror(msg);
    exit(1);
}


int main(int argc , char *argv[])
{
    int i;
	int sockfd , client_sock , c , *new_sock,*new_sock1,*new_sock2, portno;
	int client1_sock, client2_sock;
    struct sockaddr_in server , client;
	 
	 
	if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     } 

	//Create socket
    portno = atoi(argv[1]);
    sockfd = socket(AF_INET , SOCK_STREAM , 0);
    if (sockfd == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( portno);
     
    //Bind
    if( bind(sockfd,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
     
    //Listen
    listen(sockfd , 1);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
     
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);

	if( (client1_sock = accept(sockfd, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        puts("Connection accepted");
         
         //pthread_t sniffer_thread;
        new_sock1 = malloc(1);
        *new_sock1 = client1_sock;
         
		client1_handler(new_sock1);
		
    }
    else
    {
        perror("accept failed");
        return 1;
    }

	
	if( (client2_sock = accept(sockfd, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        puts("Connection accepted");
         
        //pthread_t sniffer_thread;
        new_sock2= malloc(1);
        *new_sock2 = client2_sock;
         
		client2_handler(new_sock2);
		 
        
    }
    else
    {
        perror("accept failed");
        return 1;
    }
	
	/*
    while (running_threads > 0)
	{
     sleep(1);
	}*/
    return 0;
}
 
/*
 * This will handle connection for  client 1
 * */
void client1_handler(void *sockfd)
{
    //Get the socket descriptor
	int i;
    int sock = *(int*)sockfd;
    int read_size,n;
    char *message , client_message[2000];
    
	memset(ciphertext_receiver,0,1024*1024); //clear the full receiving buffer
    message="I got your message";	//confirm message sent for each round
	
    //Receive a ciphertext from client1
    while(1) 
    {
        
		read_size = read(sock , buffer , BYTE_SIZE);
		//Send the message back to client
		if(read_size<0)
			perror("ERROR reading from socket");
		memcpy(ciphertext_receiver+received_bytes,buffer,read_size);	// copy the just received texts to global buffer
		received_bytes+=read_size;
		if(read_size==0){
			printf("executing break\n");
			break;
		}
		if(read_size==BYTE_SIZE)
			bzero(buffer,BYTE_SIZE)	;
		n=write(sock , message , strlen(message));	//send the confirmed message
		if (n < 0) perror("ERROR writing to socket");

	}	
	puts("Client 1 disconnected");
	printf("Total of %d bytes received\n",received_bytes);
	
	ciphertext_len=received_bytes-1024*2;
	memcpy(encrypted_hash_receiver,ciphertext_receiver+received_bytes-1024*2,1024); //copy the SHA256 to received buffer
	memcpy(encrypted_passwd_receiver,ciphertext_receiver+received_bytes-1024,1024); //copy the received password to buffer

	puts("File transfer finished");	
	puts("Client 1 disconnected");
	
	//Free the socket pointer
    free(sockfd);
     
    return ;
}


/*
 * This will handle connection for  client 2
 * */


void client2_handler(void *sockfd)
{
	struct timeval timeout;      
    	timeout.tv_sec = 30;
    	timeout.tv_usec = 0;
	
	int errorid = 0;
	socklen_t len = sizeof (errorid);
	int retval; 

	//Get the socket descriptor
	int i; // index for input counter
	int n=0; //return value for sending bytes
    	int sock = *(int*)sockfd;	//get the socket file descriptor from main
    	int read_size;
    	unsigned char buf[255];
	//char filepath[100];
	//unsigned char passwd[30];
    	char *message , client_message[2000];
	
	int sent_bytes=0;	// keep track of the bytes been sent
	int keep_write=1;  // determine when to quit the while loop of writing ciphertext data
	
	int selection_wrong=0;
   
	if (setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)		// set the socket sending operation timeout to 30 seconds
        error("setsockopt failed\n");	

	puts("Choose which mode to operate:");
	puts("Enter t will let server operate in trusted mode and transfer unmodified files");
	puts("Enter u will let server operate in untrusted mode and replace the file with another user selected file");
	choice=getchar();
	while(choice!='t'&&choice!='u')
	{
		puts("Wrong selection");
		selection_wrong++;
		if(selection_wrong>5){
			puts("More than 5 times selection wrong, quitting the program");
			free(sockfd);
			return;
		}
			
		puts("Please enter either t or u");
		choice=getchar();
	}
	
	
	if(choice=='t')
	{
		puts("You've chosen to forward all data directly to client 2");
		
		puts("starting to send ciphertext");
		// send the ciphtertext to client2_buffer
		while(keep_write){
			


			if(sent_bytes<ciphertext_len-BYTE_SIZE){		 // control the outputlength
				//n=write(sock,ciphertext_receiver+sent_bytes,BYTE_SIZE);
				if((n=(int)send(sock,ciphertext_receiver+sent_bytes,BYTE_SIZE,MSG_NOSIGNAL))!=BYTE_SIZE)
					error("Sending ciphertext error");

			}
			else{
				//n=write(sock,ciphertext_receiver+sent_bytes,ciphertext_len-sent_bytes);
				if((n=(int)send(sock,ciphertext_receiver+sent_bytes,ciphertext_len-sent_bytes,MSG_NOSIGNAL))!=(ciphertext_len-sent_bytes))
					error("sending ciphertext error");
				printf("the number of sent bytes: %d\n", (int)n);
				keep_write=0;
				//printf("%s\n",ciphertext_receiver+sent_bytes);
			}
			if (n < 0) 
				error("ERROR writing to socket");
			sent_bytes+=n;

		}
		printf("sent bytes: %d\n",sent_bytes);
		printf("sending ciphertext success\n");
		printf("\n");
	
		// send the SHA256 Signature
		printf("Sending the SHA256 Signature Now\n");
		n = write(sock,encrypted_hash_receiver,1024);
		if (n < 0) 
			error("ERROR writing SHA256 Signature to socket");
		else
			printf("SHA 256 Signature sent\n");
	
		// send the RSA encrypted password
		printf("Sending the RSA encrypted password now\n");
		n= write(sock, encrypted_passwd_receiver, 1024);
	
		if (n < 0) 
			error("ERROR writing RSA encrypted password to socket");
		else
			printf("RSA encrypted password sent\n");
	}
	else if(choice=='u')
	{		
		//check if client2 is still online, otherwise quit the program
		
		puts("You have chosen encrypt another random file and send it to client 2");
		
		// get password and check invalidty
		puts("Please enter your password to encrypt the random file");
		do{
    		repeat_enter_passwd=0;
    		printf("Enter a password :\n");
    		gets(passwd);
    		if(strlen(passwd)!=16){
			printf("Password length must be 16, please re-enter \n");
			repeat_enter_passwd=1;
    		}
    		else{
	    		for(i=0;passwd[i]!=0;i++){
	      			if((passwd[i]<33&&passwd[i]>0) || passwd[i]>125){
					printf("Contain illegal characters, please re-enter password: \n");
					repeat_enter_passwd=1;
					break;
					}
				}    
	     	
    		}	
    		printf("The input password is %s\n",passwd);  // print password content
   	
   

    		if(repeat_enter_passwd) {
			//free(passwd);
        		memset(passwd,0,sizeof(passwd));
				passwd_wrong_counter++;
    		}
			if(passwd_wrong_counter>3){
				puts("password entered wrong more than 3 times");
				puts("exiting the program...");
				return;
			}
    
		}while(repeat_enter_passwd);
		
		// get plaintext file path and file content
		printf("Please enter your file path below\n");
		gets(filepath);		//
	
		fp=fopen(filepath,"r");

		int wrong_file_counter=0;
		while(fp==NULL)
    		{
        		puts("\n fopen() Error!!!");
			puts("Please reenter the file path");
			memset(filepath,0,sizeof(filepath));
			gets(filepath);
			wrong_file_counter++;
			if(wrong_file_counter>3)
			{
					puts("More than 3 times wrong input, exitting the program...");  //allow no more than 3 times of wrong input
					return;
			}
			fp=fopen(filepath,"r");
		}
		fseek(fp, 0L, SEEK_END);
		sz = ftell(fp);
		fseek(fp,0,SEEK_SET);
		fread(text,sz,1,fp);
		fclose(fp); //close the plaintext pointer
		
		
		for(i=0; i<AES_BLOCK_SIZE; ++i) 
    		iv[i]=0;
	
		cipher_type = EVP_aes_128_cbc();
		
		EVP_CIPHER_CTX_init(&en);
		EVP_EncryptInit_ex(&en, cipher_type, NULL, passwd, iv);


		// We add 1 because we're encrypting a string, which has a NULL terminator and want that NULL terminator to be present when we decrypt.
		input_len=strlen(text)+1;
		printf("the plaintext length is : %d\n", input_len);

	
		ciphertext = (unsigned char *) malloc(input_len + MAX_PADDING_LEN);

        /* allows reusing of 'e' for multiple encryption cycles */
		if(!EVP_EncryptInit_ex(&en, NULL, NULL, NULL, NULL)){
			printf("ERROR in EVP_EncryptInit_ex \n");
			return;
		}

      	// starting encryption
      	int bytes_written = 0;
      	if(!EVP_EncryptUpdate(&en,
                           ciphertext, &bytes_written,
                           (unsigned char *) text, input_len) ) {
        	printf("ERROR in EVP_EncryptUpdate \n");
        	return;
      	}
      	ciphertext_len_local += bytes_written;

		// EVP_EncryptFinal_ex writes the padding. 
      	if(!EVP_EncryptFinal_ex(&en,
                              ciphertext + bytes_written,
                              &bytes_written)){
       	 	printf("ERROR in EVP_EncryptFinal_ex \n");
        	return;
      	}
      	ciphertext_len_local += bytes_written;

      	EVP_CIPHER_CTX_cleanup(&en);


      	printf("Input len: %d, ciphertext_len: %d\n", input_len, ciphertext_len_local);
		puts("");

		
		// send the ciphertext
		int ciphertext_len_sent=ciphertext_len_local;
		while(keep_write){
			if(sent_bytes<ciphertext_len_sent-BYTE_SIZE)		 // control the outputlength
				n=write(sock,ciphertext+sent_bytes,BYTE_SIZE);
			else{
				n=write(sock,ciphertext+sent_bytes,ciphertext_len_sent-sent_bytes);	
				keep_write=0;
			}
		
			if (n < 0) 
				error("ERROR writing to socket");
			sent_bytes+=n;
			
		}
		printf("sent bytes: %d\n",sent_bytes);
		printf("sending ciphertext success\n");
		printf("\n");
				
		// send the SHA256 Signature
		printf("Sending the SHA256 Signature Now\n");
		n = write(sock,encrypted_hash_receiver,1024);
		if (n < 0) 
			error("ERROR writing SHA256 Signature to socket");
		else
			printf("SHA 256 Signature sent\n");
	
		// send the RSA encrypted password
		printf("Sending the RSA encrypted password now\n");
		n= write(sock, encrypted_passwd_receiver, 1024);
	
		if (n < 0) 
			error("ERROR writing RSA encrypted password to socket");
		else
			printf("RSA encrypted password sent\n");
	}
	   

    //Free the socket pointer
    free(sockfd);
    running_threads--; 
    return;
}